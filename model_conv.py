import torch.nn as nn
from torch.autograd import Variable
from models.seq_rnn import SeqRNN
import torch
from preproc import deserialize_from_file


class ModelConv(nn.Module):
	def __init__(self, word_embed_size, char_embed_size, rnn_size, final_hidden, n_features,
				 word_vocab, char_vocab, n_layers, gpu_id, dropout_rate, conv_size):
		super().__init__()

		self.word_embed_size = word_embed_size
		self.char_embed_size = char_embed_size
		self.rnn_size = rnn_size
		self.final_hidden_size= final_hidden
		self.n_features = n_features
		self.depth = n_layers
		self.drop = dropout_rate

		self.word_rnn = SeqRNN(min(100000, len(word_vocab.itos)),
													word_embed_size, rnn_size,
								 n_layers=n_layers, dropout_rate=dropout_rate,bi=True,
							     embedding_matrix=deserialize_from_file('data/embedding_matrix.pt'))
		self.char_rnn = SeqRNN(len(char_vocab.itos), char_embed_size, rnn_size,
								 n_layers=n_layers, dropout_rate=dropout_rate,bi=True)

		self.conv_word = nn.Conv1d(rnn_size*2,conv_size,3,padding=2)

		self.conv_char = nn.Conv1d(rnn_size*2,conv_size,5,padding=5)



		self.lrelu = nn.LeakyReLU()

		self.lin = nn.Linear(conv_size*4, final_hidden)
		self.drop = nn.Dropout(dropout_rate)
		self.tanh_lin = nn.Tanh()

		self.output = nn.Linear(final_hidden, 6)

		self.sigm = nn.Sigmoid()

		self.gpu_id = gpu_id

	# inputs: (word_indices, char_indices, labels, word_lens, char_lens)
	# data: Variable(Tensor(batch_size, max_sequence_len))
	# labels: Variable(Tensor(batch_size))
	def forward(self, word_input, char_input):
		# batch x sequence_len x embed_dim
		# print("[model.fwd] word_input = ", word_input)
		output_word, hidden_word = self.word_rnn(word_input)
		output_char, hidden_char = self.char_rnn(char_input)

		output_word = torch.transpose(output_word,2,1)
		output_char = torch.transpose(output_char,2,1)

		conv_w_out = self.conv_word(output_word)
		conv_c_out = self.conv_char(output_char)

		concat = self.lrelu(torch.cat((torch.max(conv_w_out,-1)[0], 
												torch.mean(conv_w_out,-1),
												torch.max(conv_c_out,-1)[0], torch.mean(conv_c_out,-1)),-1))

		lin = self.tanh_lin(self.drop(self.lin(concat)))
		scores = self.output(lin)

		return scores, self.sigm(scores)

	def save(self, model_path):
		torch.save(self.state_dict(), model_path)

	def load(self, model_path, gpu_id):
		if gpu_id==-1:
			dev = 'cpu'
		else:
			dev = 'cuda:%d'%gpu_id
		dev_map = {'cpu':dev}
		for i in range(6):
			dev_map['cuda:%d'%i]=dev
		self.load_state_dict(torch.load(model_path, map_location = dev_map))

	def zero_hidden(self, batch_size, gpu_id):
		return self.word_rnn.zero_hidden(batch_size, gpu_id), \
			self.char_rnn.zero_hidden(batch_size, gpu_id)

	def rnn_params(self):
		ret = []
		ret.extend(self.word_rnn.parameters())
		ret.extend(self.char_rnn.parameters())
		return ret


if __name__ == '__main__':
	Model.test_forward()
