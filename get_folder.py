import argparse
import os


if __name__=='__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('folder', type=str)
	parser.add_argument('-titan',type=int,default=1)
	args = parser.parse_args()
	os.system('scp -r titan%d:/data/experimental/nlp-group/toxic/%s .'%(args.titan,
		args.folder))