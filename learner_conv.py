import torch
import torch.nn as nn
import numpy as np
import sys
import os
import logger
from torch.autograd import Variable
from sklearn.metrics import roc_auc_score, confusion_matrix
from preproc import serialize_to_file
import pandas as pd

class LearnerConv(object):

	def __init__(self, model, id, class_ind, gpu_id, dataset, optimizer, lr_rate, 
			max_clip_norm, model_path, log_mode='w'):
		self.model = model
		self.class_ind = class_ind
		self.train_set, self.val_set = dataset
		self.optim = optimizer(model.parameters(), lr=lr_rate)
		self.class_dist = self.train_set.class_dist[self.class_ind]
		self.val_dist = self.val_set.class_dist[self.class_ind]
		self.weights_train = Variable(torch.FloatTensor([self.class_dist,1-self.class_dist]))
		self.weights_val = Variable(torch.FloatTensor([self.val_dist,1-self.val_dist]))
		if gpu_id>=0:
			self.weights_train = self.weights_train.cuda(gpu_id)
			self.weights_val = self.weights_val.cuda(gpu_id)
		self.loss_weighted = nn.MultiLabelSoftMarginLoss()
		self.loss_val = self.loss_weighted
		self.gpu_id = gpu_id
		self.id = str(id)+'_'+str(class_ind)
		self.model_path = os.path.join(model_path, self.id)
		if not os.path.exists(self.model_path):
			os.makedirs(self.model_path)
		self.max_clip_norm = max_clip_norm
		if log_mode is 'w':
			logger.log(self.id, 'info', mode='w', verbose=False)


	def train(self, epochs, batch_size):
		best_val = np.inf
		for epoch in range(epochs):
			total_iters = 0
			epoch_iters = 0
			epoch_loss = 0
			iters = sum([self.train_set.part_len(part) for part in range(self.train_set.num_parts)])//batch_size
			for it in range(iters):
				part=np.random.randint(self.train_set.num_parts)
				part_len = self.train_set.part_len(part)
				ids = np.random.permutation(part_len)
				i=0
				self.model.zero_grad()
				word_input, char_input, labels, word_lens, char_lens, input_examples, feat_vector = \
					self.train_set.get_batch_input(part,ids[i*batch_size:(i+1)*batch_size])

				if self.gpu_id >= 0:
					word_input = word_input.cuda(self.gpu_id)
					char_input = char_input.cuda(self.gpu_id)
					labels = labels.cuda(self.gpu_id) 
					feat_vector = feat_vector.cuda(self.gpu_id)


				scores, soft_scores = self.model(word_input, char_input)
				loss_weighted = self.loss_weighted(scores,labels)
				loss_weighted.backward()
				epoch_loss+=loss_weighted.data[0]

				#clip gradients
				torch.nn.utils.clip_grad_norm(self.model.rnn_params(), self.max_clip_norm)

				self.optim.step()
				logger.log(self.id, 'train', epoch=epoch, it=it, 
					loss=loss_weighted.data[0], part=part)
				sys.stdout.flush()
				total_iters+=1
				epoch_iters+=1

			val_loss = self.eval(batch_size)
			if val_loss<best_val:
				best_val = val_loss
				self.model.save(os.path.join(self.model_path,'best.pt'))
			logger.log(self.id, 'val_med', epoch=epoch, loss=val_loss)
			logger.log(self.id, 'train_med', epoch=epoch, loss=epoch_loss*1.0/epoch_iters)

			self.model.save(os.path.join(self.model_path,'%s.pt'%epoch))

	def eval(self, batch_size, epoch=0):
		self.model.eval()
		total_iters = 0
		total_loss = 0
		total_scores = [[],[]]
		hidden_state = {}
		for part in range(self.val_set.num_parts):
			part_len = self.val_set.part_len(part)
			iters = (part_len // batch_size) + (part_len%batch_size!=0)
			
			ids = np.random.permutation(part_len)
			for i in range(iters):

				word_input, char_input, labels, word_lens, char_lens, input_examples, feat_vector = \
					self.val_set.get_batch_input(part, ids[i*batch_size:(i+1)*batch_size],
												 requires_grad = False)

				if self.gpu_id >= 0:
					word_input = word_input.cuda(self.gpu_id)
					char_input = char_input.cuda(self.gpu_id)
					labels = labels.cuda(self.gpu_id)
					feat_vector = feat_vector.cuda(self.gpu_id)


				scores, soft_scores= self.model(word_input, char_input)

				loss_weighted = self.loss_val(scores,labels).data[0]
				logger.log(self.id, 'val', it=total_iters, 
					part=part, loss = loss_weighted)
				total_loss += loss_weighted
				total_iters+=1


		val_loss = total_loss / total_iters

		self.model.train()

		#logger.log(self.id, 'val_med', epoch=epoch, loss=val_loss, roc_score=roc_score,
		#		z_tn=conf_mat[0][0], z_fp=conf_mat[0][1], z_fn=conf_mat[1][0], z_tp=conf_mat[1][1])

		return val_loss

	def to_csv(self, dataset, batch_size):
		self.model.eval()
		probs = {}
		hidden_state = {}
		for part in range(dataset.num_parts):
			part_len = dataset.part_len(part)
			iters = (part_len // batch_size) + (part_len%batch_size!=0)
			
			ids = list(range(part_len))
			for i in range(iters):
				word_input, char_input, labels, word_lens, char_lens, input_examples, feat_vector = \
					dataset.get_batch_input(part, ids[i*batch_size:(i+1)*batch_size],
												 requires_grad = False)

				if self.gpu_id >= 0:
					word_input = word_input.cuda(self.gpu_id)
					char_input = char_input.cuda(self.gpu_id)
					feat_vector = feat_vector.cuda(self.gpu_id)

				scores, soft_scores = self.model(word_input, char_input)
				soft_scores = soft_scores.cpu()

				for j, ex in enumerate(input_examples):
					if ex.id in probs:
						probs[ex.id] = np.maximum(probs[ex.id], soft_scores[j].data.numpy())
					else:
						probs[ex.id] = soft_scores[j].data.numpy()

				print('part[%d] it/iters[%d/%d]'%(part,i,iters))
				sys.stdout.flush()

		pd.DataFrame.from_dict(probs, orient='index').round(6).to_csv('submission.csv', 
			header=[str(x) for x in range(6)])
		self.model.train()
