import numpy as np
import xgboost as xgb
from preproc import deserialize_from_file, serialize_to_file, split_data
from argparse import ArgumentParser
import pickle
import sys
from preproc import deserialize_from_file

RAW=False

def get_emb(*datasets):
	ret = []
	for d in datasets:
		ds = deserialize_from_file('data/%s_embeddings.bin'%d)
		mat = np.zeros([len(ds),len(ds[0][2][0])])
		labels = np.zeros([len(ds),6])
		for (i,ex) in enumerate(ds):
			mat[i]=ex[2][0]
			if d!='test':
				labels[i]=ex[1]
		ret.append((mat,labels))
	print('ds loaded')
	sys.stdout.flush()
	if len(ret)==1:
		return ret[0]
	else:
		return ret


def train(name, emb):
	if emb:
		train_ds= get_emb('train')
	for i in range(6):
		if not emb:
			if RAW:
				train_set = xgb.DMatrix('data/train_%d.dmat'%i)
				val_set = xgb.DMatrix('data/val_%d.dmat'%i)
			else:
				with open('data/train_%d.bin'%i, 'rb') as f:
					train_set = pickle.load(f)
				with open('data/val_%d.bin'%i, 'rb') as f:
					val_set = pickle.load(f)
		else:
			train_set = (train_ds[0][:140000],train_ds[1][:140000,i])
			val_set = (train_ds[0][140000:],train_ds[1][140000:,i])
			if RAW:
				train_set = xgb.DMatrix(train_set[0], label=train_set[1])
				val_set = xgb.DMatrix(val_set[0], label=val_set[1])
		params = {'max_depth':40, 'silent':0, 'objective':'binary:logistic',
		'tree_method': 'exact', 'eval_metric':'auc', 'predictor':'cpu_predictor', 
		'n_jobs':8, 'min_child_weight':0, 'reg_lambda':0.1, 'scale_pos_weight':20,
		'random_state':0, 'learning_rate':0.1, 'early_stopping_rounds':20,
		'n_estimators':100 }
		if RAW:
			rounds = 200
			model = xgb.train(params, train_set, rounds,
			 evals=[(val_set,'val')], verbose_eval=True)
			model.save_model('runs/xgb/%s_%d.model'%(name,i))
		else:
			model = xgb.XGBClassifier(**params)
			model.fit(train_set[0], train_set[1], eval_set=[val_set], 
				eval_metric='auc')
			with open('runs/xgb/%s_%d.model'%(name,i), 'wb') as f:
				pickle.dump(model,f)

def test(name):
	with open('data/train.bin','rb') as f:
		test_set = pickle.load(f)
	for i in range(6):
		print('Set [%d]'%i)
		sys.stdout.flush()
		if RAW:
			model = xgb.Booster({'nthread':4})
			model.load_model('runs/xgb/%s_%d.model'%(name,i))
			pred = model.predict(test_set)
		else:
			with open('runs/xgb/%s_%d.model'%(name,i),'rb') as f:
				model = pickle.load(f)
			pred =model.predict_proba(test_set)
		np.savetxt('%s_%d.csv'%(name,i), pred[:,1], fmt='%.6f')




def main():
	parser = ArgumentParser()
	parser.add_argument('-emb', action='store_true')
	parser.add_argument('-load',type=str)
	parser.add_argument('-name',type=str,default='xgb')
	args = parser.parse_args()
	if args.load is not None:
		test(args.load, args.emb)
	else:
		train(args.name, args.emb)


if __name__=='__main__':
	main()