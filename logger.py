import argparse
import matplotlib.pyplot as plt
import sys
import os


log_path='logs/'

def convert(s):
	try:
		return int(s)
	except:
		try:
			return float(s)
		except:
			return s


def log(id, log_type,verbose=True,mode='a', **kargs):
	if not os.path.exists(log_path):
			os.makedirs(log_path)
	f = open(log_path+id+'.log',mode) 
	line = log_type+' '
	for key in sorted(kargs.keys()):
		val = kargs[key]
		if type(val) is float:
			line+='%s[%.5f] '%(key,val)
		else:
			line+='%s[%s] '%(key,val)
	if verbose:
		print(line)
		sys.stdout.flush()
	print(line,file=f)
	f.close()



def parse_log(file, flt):
	f = open(file,'r')
	ret = {}
	for line in f.readlines():
		line = [s.strip() for s in line.strip().split(' ')]
		if line[0]!=flt:
			continue
		if line[0] not in ret:
			ret[line[0]]={}
		for data in line[1:]:
			args=data.split('[')
			if len(args)<2:
				continue
			key, val = args[0],args[1]
			val = val[:-1]
			if key not in ret[line[0]]:
				ret[line[0]][key]=[convert(val)]
			else:
				ret[line[0]][key].append(convert(val))
	return ret


def plot(data):
	plt.plot(data)
	plt.show()

if __name__=='__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('logs',type=str)
	parser.add_argument('-plot', type=str)
	args=parser.parse_args()
	if args.plot is not None:
		plot(parse_log(args.logs,args.plot)[args.plot]['loss'])