from preproc import deserialize_from_file, serialize_to_file, split_data
import numpy as np
from scipy.sparse import csr_matrix, lil_matrix
from nltk import word_tokenize
from collections import Counter
import sys
import xgboost as xgb
import pickle


def main():
	ds = {}
	vocab = deserialize_from_file('data/vocab.pt')
	total = sum(vocab.freqs.values())
	for c in ['train','val','test']:
		ds[c] = deserialize_from_file('data/'+c+'.pt')

	dist = ds['train'].class_dist

	for c in ['train','val','test']:
		mat = lil_matrix((len(ds[c]),len(vocab.itos)), dtype=np.float)
		labels = np.zeros([len(ds[c]),6])
		weights = np.zeros([len(ds[c]),6], dtype=np.float)
		for (i,ex) in enumerate(ds[c].examples):
			words = Counter(word_tokenize(ex.data))
			for word in words:
				mat[i,vocab.stoi[word]] = words[word]*1.0/total
			if c != 'test':
				for j in range(len(ex.labels)):
					labels[i][j] = ex.labels[j]
					weights[i][j] = labels[i][j]*(1-dist[j]) + (1-labels[i][j])*dist[j] 
			if i%100 is 0:
				print(i)
				sys.stdout.flush()
		if c!='test':
			for i in range(6):
				dmat = xgb.DMatrix(csr_matrix(mat), label = labels[:,i], 
					weight=weights[:,i])
				dmat.save_binary('data/%s_%d.dmat'%(c,i))
				with open('data/%s_%d.bin'%(c,i),'wb') as f:
					pickle.dump((csr_matrix(mat),labels[:,i]),f)
		else:
			dmat = xgb.DMatrix(csr_matrix(mat))
			dmat.save_binary('data/test.dmat')
			with open('data/test.bin','wb') as f:
				pickle.dump(csr_matrix(mat),f)


def main2():
	ds = {}
	vocab = deserialize_from_file('data/vocab.pt')
	total = sum(vocab.freqs.values())
	for c in ['train','val','test']:
		ds[c] = deserialize_from_file('data/'+c+'.pt')

	dist = ds['train'].class_dist

	for c in ['train','val','test']:
		vocab_size = len(vocab.itos)
		mat = lil_matrix((len(ds[c]),vocab_size*vocab_size), dtype=np.float)
		labels = np.zeros([len(ds[c]),6])
		weights = np.zeros([len(ds[c]),6], dtype=np.float)
		for (i,ex) in enumerate(ds[c].examples):
			words = word_tokenize(ex.data)
			for k in range(len(words)-1):
				mat[i,vocab.stoi[words[k]]*vocab_size+vocab.stoi[words[k+1]]] += 1
			if c != 'test':
				for j in range(len(ex.labels)):
					labels[i][j] = ex.labels[j]
					weights[i][j] = labels[i][j]*(1-dist[j]) + (1-labels[i][j])*dist[j] 
			if i%100 is 0:
				print(i)
				sys.stdout.flush()
		if c!='test':
			for i in range(6):
				dmat = xgb.DMatrix(csr_matrix(mat), label = labels[:,i],
				 weight=weights[:,i])
				dmat.save_binary('data/%s_%d.dmat'%(c,i))
				with open('data/%s_%d.bin'%(c,i),'wb') as f:
					pickle.dump((csr_matrix(mat),labels[:,i]),f)
		else:
			dmat = xgb.DMatrix(csr_matrix(mat))
			dmat.save_binary('data/test.dmat')
			with open('data/test.bin','wb') as f:
				pickle.dump(csr_matrix(mat),f)

main2()