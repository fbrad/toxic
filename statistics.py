import pandas
import argparse
import numpy as np

cols=['toxic', 'severe_toxic', 'obscene', 'threat', 
'insult', 'identity_hate']


def base(data,labels):
	nr_elems = len(data['id'])
	print('nr_elems: [%d]'%nr_elems)
	if labels:
		toxic = data[cols].max(axis=1)
		print('toxic: [%.6f]'%((toxic.sum()*0.1/nr_elems)*100.0))
	lens = data['comment_text'].str.len()
	print('mean-len [%.3f] std-len [%.3f] median-len[%d] max-len [%d]'%(
		lens.mean(), lens.std(),lens.median(),lens.max()))

def words(data,labels):
	nr_elems = len(data['id'])
	words=[]
	words_toxic=[]
	hist = set()
	dw_total = 0
	dw_toxic = 0
	max_len = 0
	max_word = ''
	if labels:
		toxic=data[cols].max(axis=1)
	for (i,comment) in enumerate(data['comment_text']):
		nr = 0
		if type(comment) is not float:
			nr = comment.count(' ')
		words.append(nr)
		if labels and toxic[i]>0:
			words_toxic.append(nr)
		if type(comment) is float:
			continue
		for word in comment.split(' '):
			if len(word) is 0:
				continue
			if len(word)>max_len:
				max_len = len(word)
				max_word = word
			if word not in hist:
				dw_total+=1
				hist.add(word)
				if labels and toxic[i]>0:
					dw_toxic+=1

	print('max_len[%d]'%max_len)
	print('mean-w [%.3f] std-w[%.3f] median-w[%d] max-w[%d]'%(np.mean(words),
		np.std(words),np.median(words),np.max(words)))
	if labels:
		print('mean-wt [%.3f] std-wt[%.3f] median-wt[%d] max-wt[%d]'%(
			np.mean(words_toxic),np.std(words_toxic),np.median(words_toxic),
			np.max(words_toxic)))
	print('Different words: [%d]'%dw_total)
	if labels:
		print('Toxic different word: [%d]'%dw_toxic)


def main(args):
	data = pandas.read_csv(args.path)
	if args.base:
		base(data,args.labels)
	if args.words:
		words(data,args.labels)


parser = argparse.ArgumentParser()
parser.add_argument('path',type=str)
parser.add_argument('--base',action='store_true')
parser.add_argument('--words',action='store_true')
parser.add_argument('--labels',action='store_true')
args =parser.parse_args()
main(args)