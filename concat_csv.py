import pandas as pd

files = ['xgb_0','xgb_1','xgb_2','xgb_3','xgb_4','xgb_5']

def main():
	test_set = pd.read_csv('data/test.csv')
	csv = open('submission.csv','w')
	print("id,toxic,severe_toxic,obscene,threat,insult,identity_hate",file=csv)
	lines = []
	for file in files:
		with open(file+'.csv','r') as f:
			lines.append([line.strip() for line in f.readlines()])
	for (i,id) in enumerate(test_set['id']):
		print('%s,%s,%s,%s,%s,%s,%s'%(id,lines[0][i],lines[1][i],lines[2][i],
			lines[3][i],lines[4][i],lines[5][i]), file=csv)
	csv.close()



if __name__=='__main__':
	main()