import numpy as np
import pandas as pd

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import cross_val_score
from scipy.sparse import hstack, csr_matrix
import xgboost as xgb
import pickle
from sklearn.utils import shuffle

class_names = ['toxic', 'severe_toxic', 'obscene', 'threat', 
							'insult', 'identity_hate']

train = pd.read_csv('data/train.csv').fillna(' ')
test = pd.read_csv('data/test.csv').fillna(' ')


labels = np.zeros([len(train),6])
for (i,c) in enumerate(class_names):
	labels[:,i]=train[c]


train_text = train['comment_text']
test_text = test['comment_text']
all_text = pd.concat([train_text, test_text])

word_vectorizer = TfidfVectorizer(
	sublinear_tf=True,
	strip_accents='unicode',
	analyzer='word',
	token_pattern=r'\w{1,}',
	stop_words='english',
	ngram_range=(1, 1),
	max_features=10000)
word_vectorizer.fit(all_text)
train_word_features = word_vectorizer.transform(train_text)
test_word_features = word_vectorizer.transform(test_text)

char_vectorizer = TfidfVectorizer(
	sublinear_tf=True,
	strip_accents='unicode',
	analyzer='char',
	stop_words='english',
	ngram_range=(2, 6),
	max_features=50000)
char_vectorizer.fit(all_text)
train_char_features = char_vectorizer.transform(train_text)
test_char_features = char_vectorizer.transform(test_text)




train_stack=csr_matrix(hstack([train_char_features, 
	train_word_features]))
num_train=int(0.9*train_stack.shape[0])
train_features = train_stack[:num_train]
train_labels = labels[:num_train]
val_features = train_stack[num_train:]
val_labels = labels[num_train:]
test_features = csr_matrix(hstack([test_char_features, test_word_features]))


datasets = {'train':(train_features,train_labels),
						'val':(val_features,val_labels),
						'test':test_features}

for ds_name, ds in datasets.items():
	if ds_name!='test':
		for i in range(6):
			mat = xgb.DMatrix(ds[0],label=ds[1][:,i])
			mat.save_binary('data/%s_%d.dmat'%(ds_name,i))
			print(ds[1][:,i].shape)
			with open('data/%s_%d.bin'%(ds_name,i),'wb') as f:
				pickle.dump((ds[0],ds[1][:,i]),f)
	else:
		mat = xgb.DMatrix(ds)
		mat.save_binary('data/test.dmat')
		with open('data/test.bin','wb') as f:
			pickle.dump(ds,f)

