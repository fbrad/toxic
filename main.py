import argparse
import os
import csv
import torch
import pickle as cPickle
from random import shuffle
from torchtext.vocab import Vocab
from torch.autograd import Variable
from preproc import deserialize_from_file, serialize_to_file, split_data
from model import Model
from model_conv import ModelConv
from learner import Learner
from learner_conv import LearnerConv

parser = argparse.ArgumentParser(description='main.py',
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument('-data', type=str, default='data/',
                    help='path to data.train.pt, data.val.pt and data.vocab.pt')

parser.add_argument('-save_to', type=str, default='runs/',
                        help='path to saved .pt models')

parser.add_argument('-id', type=int, default=0, help='model id')

parser.add_argument('-class_ind', type=int, default=0, help = 'class specialization')

parser.add_argument('-rnn_size', type=int, default=256, help = "encoder hidden state size")
parser.add_argument('-final_hidden_size', type=int, default=50, help = "size of space where \
												hidden/features are projected")
parser.add_argument('-num_features', type=int, default=2, help = "number of extra features")

parser.add_argument('-lr_rate', type=float, default=1e-3, help = "learning_rate")
parser.add_argument('-dropout_rate', type=float, default=0.2, help = "dropout rate")

parser.add_argument('-word_embed_size', type=int, default=64, help = "word embedding size")
parser.add_argument('-max_clip_norm', type=float, default=5.0, help = "max norm of rnn gradients")


parser.add_argument('-char_embed_size', type=int, default=64, help = "char embedding size")

parser.add_argument('-gpu_id', type=int, default=-1, help = "id of gpu (-1 = CPU)")
parser.add_argument('-epochs', type=int, default=100, help = "number of epochs to train")

parser.add_argument('-batch_size', type=int, default=128, help = "number of epochs to train")

parser.add_argument('-n_layers', type=int, default=2, help = "number of rnn layers")

parser.add_argument('-max_words', type=int, default=100, help = "maximum number of words in an input sequence")
parser.add_argument('-max_word_chars', type=int, default=20, help = "maximum number of characters in a word")

parser.add_argument('-conv_size', type=int, default=128, help = "convolutional filter size")


parser.add_argument('-load', type=str, help="model to be loaded")

parser.add_argument('-test', action='store_true', help="run the model in test mode and produce a .csv")

parser.add_argument('-eval', action='store_true', help="eval on validation")

parser.add_argument('-save_emb', action='store_true', help="save embeddings option")

parser.add_argument('-conv', action='store_true', help='use convolutional model')






if __name__ == '__main__':
	args = parser.parse_args()

	vocab = deserialize_from_file(args.data + 'vocab.pt')
	train = deserialize_from_file(args.data + 'train.pt')
	val = deserialize_from_file(args.data + 'val.pt')

	if args.conv:
		model = ModelConv(args.word_embed_size, args.char_embed_size, args.rnn_size,
					  args.final_hidden_size, args.num_features,
					  train.word_vocab, train.char_vocab, args.n_layers, args.gpu_id,
					  args.dropout_rate,args.conv_size)
	else:
		model = Model(args.word_embed_size, args.char_embed_size, args.rnn_size,
					  args.final_hidden_size, args.num_features,
					  train.word_vocab, train.char_vocab, args.n_layers, args.gpu_id,
					  args.dropout_rate)

	if args.load is not None:
		model.load(args.load, args.gpu_id)
		log_mode = 'a'
	else:
		log_mode = 'w'

	if args.gpu_id >= 0:
		model.cuda(args.gpu_id)
	else:
		model.cpu()

	if args.conv:
		learner = LearnerConv(model, args.id, args.class_ind, args.gpu_id, (train,val), 
										torch.optim.Adam, args.lr_rate, 
                    args.max_clip_norm, args.save_to, log_mode)
	else:
		learner = Learner(model, args.id, args.class_ind, args.gpu_id, (train,val), 
										torch.optim.Adam, args.lr_rate, 
                    args.max_clip_norm, args.save_to, log_mode)
	if args.test:
		test = deserialize_from_file(args.data+'test.pt')
		learner.to_csv(test, args.batch_size)
	elif args.eval:
		learner.eval(args.batch_size)
	elif args.save_emb:
		train = deserialize_from_file(args.data+'train.pt')
		learner.save_comment_embedding(train, 32)
	else:
		learner.train(args.epochs,args.batch_size)
