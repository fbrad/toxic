import string
import torch
from torchtext.vocab import Vocab
from torch.autograd import Variable
from collections import Counter
from nltk import word_tokenize

class Example:
	#entry: CSV line
	def __init__(self, entry):
		if type(entry) is Example:
			self.id = entry.id
			self.data = entry.data
			self.labels = entry.labels
			self.feature_vec = entry.feature_vec
		else:
			self.id = entry[0]
			self.data = entry[1]
			if len(entry)>6:
				self.labels = [int(l) for l in entry[2:8]]
				self.feature_vec = [float(x) for x in entry[8:]]
			else:
				self.labels = []
				self.feature_vec = [float(x) for x in entry[2:]]

	def is_toxic(self):
		return sum(self.labels) > 0

	def __repr__(self):
		return str(self.id) + ' ' + self.data + str(self.labels)



class Dataset:
	#examples: list of Example instances
	def __init__(self, examples, word_vocab, max_words, max_word_chars):
		self.examples = examples
		self.max_words = max_words
		self.max_word_chars = max_word_chars
		self.num_features = len(examples[0].feature_vec)
		self.examples_part = []
		for example in examples:
			words = word_tokenize(example.data)
			if len(words) is 0:
				words.append('none')
			i = 0
			words = [word[0:max_word_chars] for word in words]
			while i*max_words<len(words):
				if len(self.examples_part) is i:
					self.examples_part.append([])
				e = Example(example)
				e.data = ' '.join(words[i*max_words:(i+1)*max_words])
				if (i+1)*max_words<len(words):
					e.data+=' '
				self.examples_part[i].append(e)
				i+=1
		self.id_to_example = {(example.id, example) for example in examples}
		self.word_vocab = word_vocab
		self.char_vocab = Vocab(Counter(list(string.printable)))


	#splits dataset evenly by toxicity
	#train_per: percentage of data held for training
	def split_train_val(self, train_per = 0.9):
		#assert train_per >= 0 and train_per <= 1
		toxic = [example for example in self.examples if example.is_toxic()]
		non_toxic = [example for example in self.examples if not example.is_toxic()]

		train_toxic_idx = int(train_per * len(toxic))
		train_non_toxic_idx = int(train_per * len(non_toxic))
		train_toxic, val_toxic = toxic[:train_toxic_idx], toxic[train_toxic_idx:]
		train_non_toxic, val_non_toxic = non_toxic[:train_non_toxic_idx], \
										 non_toxic[train_non_toxic_idx:]

		return Dataset(train_non_toxic + train_toxic, self.word_vocab, self.max_words, self.max_word_chars), \
				Dataset(val_non_toxic + val_toxic, self.word_vocab, self.max_words, self.max_word_chars)

	'''
	batch_ids = indices of examples to be retrieved
	returns: input for word-rnn: Variable(torch.LongTensor(batch x t1)),
				input for char-rnn: Variable(torch.LongTensor(batch x t2)),
				labels: Variable(torch.LongTensor(batch x 6))
				word_lengths: list(batch)
				char_lengths: list(batch)
	'''

	def stoi(self,word):
		if word not in self.word_vocab.word_index:
			return 0
		else:
			return self.word_vocab.word_index[word]

	def get_batch_input(self, part, batch_ids, requires_grad = True):
		max_word_len, max_char_len = self.max_words, self.max_words*(self.max_word_chars+1)
		input_examples = [self.examples_part[part][idx] for idx in batch_ids]
		word_tokens = [word_tokenize(ex.data) for ex in input_examples]
		word_lens = [min(len(token),max_word_len) for token in word_tokens]
		char_lens = [min(len(ex.data),max_char_len) for ex in input_examples]
		char_pad = self.char_vocab.stoi['<pad>']
		batch_size = len(batch_ids)

		word_input = torch.LongTensor(batch_size, max_word_len).fill_(0)
		char_input = torch.LongTensor(batch_size, max_char_len).fill_(char_pad)
		labels = torch.FloatTensor(batch_size, 6).fill_(0)
		features = torch.FloatTensor(batch_size, self.num_features).fill_(0.0)

		for idx, ex in enumerate(input_examples):
			word_ids = [self.stoi(tok) for tok in word_tokens[idx]]
			#print("Example: ", ex.data)
			if word_lens[idx] > 0:
				word_input[idx, 0:word_lens[idx]] = torch.LongTensor(word_ids[:word_lens[idx]])
			else:
				word_input[idx, 0] = 1
				word_lens[idx] = 1

			char_ids = [self.char_vocab.stoi[c] for c in list(ex.data)]
			if char_lens[idx] > 0:
				char_input[idx, 0:char_lens[idx]] = torch.LongTensor(char_ids[:char_lens[idx]])
			else:
				char_input[idx, 0] = 1
				char_lens[idx] = 1

			if len(ex.labels) is not 0:
				labels[idx] = torch.LongTensor(ex.labels)

			for fidx, feat in enumerate(ex.feature_vec):
				features[idx, fidx] = feat

		word_input, char_input = Variable(word_input), Variable(char_input)
		labels = Variable(labels)
		features = Variable(features)

		return (word_input, char_input, labels, word_lens, char_lens, input_examples, features)

	def get_example_by_id(self, id):
		assert id < len(self.examples), "id out of bounds"
		return self.examples[id]

	def get_example_by_kaggle_id(self, kaggle_id):
		if kaggle_id in self.id_to_example:
			return self.id_to_example[kaggle_id]
		return None

	@property
	def labels_size(self):
		return len(self.examples[0].labels)

	@property
	def num_parts(self):
		return len(self.examples_part)

	def __len__(self):
		return len(self.examples)

	def part_len(self, part):
		return len(self.examples_part[part])

	@property
	def class_dist(self):
		ret = [0]*self.labels_size
		for example in self.examples:
			for i, label in enumerate(example.labels):
				ret[i]+=label
		return [x*1.0/len(self.examples) for x in ret]



#if __name__ == '__main__':

	#entries = preproc.read_csv_file('data/train_preproc.csv')
	#examples = [Example(entry) for entry in entries[0:100]]
	#word_vocab = deserialize_from_file("data/vocab.pt")

	#d = Dataset(examples, word_vocab)

	#d1, d2 = d.split_train_val()
