from models.seq_rnn import SeqRNN
import preproc
from torch.autograd import Variable
import torch
from dataset import Dataset
from preproc import deserialize_from_file
import logger
import numpy as np

if __name__ == '__main__':
	for epoch in range(10):
		for it in range(20):
			loss = np.random.rand()
			logger.log(0,'train',epoch=epoch, it=it, loss=loss)
	