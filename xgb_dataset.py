from preproc import deserialize_from_file, serialize_to_file, split_data
import sys
import numpy as np

datasets = ['train','val','test']

def main():
	for d in datasets:
		print(d)
		sys.stdout.flush()
		ds = deserialize_from_file('data/%s_embeddings.pt'%d)
		mat = np.zeros([len(ds),len(ds[0][2])])
		labels = np.zeros([len(ds),6])
		for (i,ex) in enumerate(ds):
			mat[i]=ex[2]
			if d!='test':
				labels[i]=ex[1]




		




if __name__=='__main__':
	main()