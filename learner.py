import torch
import torch.nn as nn
import numpy as np
import sys
import os
import logger
from torch.autograd import Variable
from sklearn.metrics import roc_auc_score, confusion_matrix
from preproc import serialize_to_file
import pandas as pd

class Learner(object):

	def __init__(self, model, id, class_ind, gpu_id, dataset, optimizer, lr_rate, 
			max_clip_norm, model_path, log_mode='w'):
		self.model = model
		self.class_ind = class_ind
		self.train_set, self.val_set = dataset
		self.optim = optimizer(model.parameters(), lr=lr_rate)
		self.class_dist = self.train_set.class_dist[self.class_ind]
		self.val_dist = self.val_set.class_dist[self.class_ind]
		self.weights_train = Variable(torch.FloatTensor([self.class_dist,1-self.class_dist]))
		self.weights_val = Variable(torch.FloatTensor([self.val_dist,1-self.val_dist]))
		if gpu_id>=0:
			self.weights_train = self.weights_train.cuda(gpu_id)
			self.weights_val = self.weights_val.cuda(gpu_id)
		self.loss_weighted = nn.CrossEntropyLoss(self.weights_train)
		self.loss_val = nn.CrossEntropyLoss(self.weights_val)
		self.gpu_id = gpu_id
		self.id = str(id)+'_'+str(class_ind)
		self.model_path = os.path.join(model_path, self.id)
		if not os.path.exists(self.model_path):
			os.makedirs(self.model_path)
		self.max_clip_norm = max_clip_norm
		if log_mode is 'w':
			logger.log(self.id, 'info', mode='w', verbose=False)


	def train_shuffle(self, epochs, batch_size):
		best_val = np.inf
		for epoch in range(epochs):
			total_iters = 0
			epoch_iters = 0
			epoch_loss = 0
			hidden_state = {}
			iters = sum([self.train_set.part_len(part) for part in range(self.train_set.num_parts)])//batch_size
			for it in range(iters):
				part=np.random.randint(self.train_set.num_parts)
				part_len = self.train_set.part_len(part)
				ids = np.random.permutation(part_len)
				i=0
				self.model.zero_grad()

				h_w, h_c = self.model.zero_hidden(len(ids[i*batch_size:(i+1)*batch_size]), self.gpu_id)

				word_input, char_input, labels, word_lens, char_lens, input_examples, feat_vector = \
					self.train_set.get_batch_input(part,ids[i*batch_size:(i+1)*batch_size])


				

				if self.gpu_id >= 0:
					word_input = word_input.cuda(self.gpu_id)
					char_input = char_input.cuda(self.gpu_id)
					labels = labels.cuda(self.gpu_id) 
					feat_vector = feat_vector.cuda(self.gpu_id)


				if len(h_w)==1:
					h_w=h_w[0]
				if len(h_c)==1:
					h_c=h_c[0]

				scores, soft_scores, h_w, h_c = self.model(word_input, char_input, \
					labels, word_lens, char_lens,None,None, feat_vector)
				labels = labels[:,self.class_ind]
				loss_weighted = self.loss_weighted(scores,labels)
				loss_weighted.backward()
				epoch_loss+=loss_weighted.data[0]

				if type(h_w) is not tuple:
					h_w = [h_w]
				if type(h_c) is not tuple:
					h_c = [h_c]
				for l, ex in enumerate(input_examples):
					if ex.id not in hidden_state:
						hidden_state[ex.id]=[[None]*len(h_w),[None]*len(h_c)]
					for k in range(len(h_w)):
						hidden_state[ex.id][0][k] = h_w[k][:,l,:].data
					for k in range(len(h_c)):
						hidden_state[ex.id][1][k] = h_c[k][:,l,:].data


				#clip gradients
				torch.nn.utils.clip_grad_norm(self.model.parameters(), self.max_clip_norm)

				self.optim.step()
				logger.log(self.id, 'train', epoch=epoch, it=it, 
					loss=loss_weighted.data[0], part=part)
				sys.stdout.flush()
				total_iters+=1
				epoch_iters+=1

			val_loss, roc_score,conf_mat = self.eval(batch_size)
			if val_loss<best_val:
				best_val = val_loss
				self.model.save(os.path.join(self.model_path,'best.pt'))
			logger.log(self.id, 'val_med', epoch=epoch, loss=val_loss, roc_score=roc_score,
				z_tn=conf_mat[0][0], z_fp=conf_mat[0][1], z_fn=conf_mat[1][0], z_tp=conf_mat[1][1])
			logger.log(self.id, 'train_med', epoch=epoch, loss=epoch_loss*1.0/epoch_iters)

			self.model.save(os.path.join(self.model_path,'%s.pt'%epoch))

	def train(self, epochs, batch_size):
		best_val = np.inf
		for epoch in range(epochs):
			total_iters = 0
			epoch_iters = 0
			epoch_loss = 0
			hidden_state = {}
			for part in range(self.train_set.num_parts):
				part_len = self.train_set.part_len(part)
				ids = np.random.permutation(part_len)
				iters = (part_len // batch_size)+(part_len%batch_size!=0)
				for i in range(iters):
					self.model.zero_grad()

					h_w, h_c = self.model.zero_hidden(len(ids[i*batch_size:(i+1)*batch_size]), self.gpu_id)

					word_input, char_input, labels, word_lens, char_lens, input_examples, feat_vector = \
						self.train_set.get_batch_input(part,ids[i*batch_size:(i+1)*batch_size])


					for l, ex in enumerate(input_examples):
						if ex.id in hidden_state:
							for k in range(len(h_w)):
								h_w[k][:,l,:] = hidden_state[ex.id][0][k]
							for k in range(len(h_c)):
								h_c[k][:,l,:] = hidden_state[ex.id][1][k]

					for k in range(len(h_w)):
						h_w[k] = Variable(h_w[k])
						if self.gpu_id>=0:
							h_w[k] = h_w[k].cuda(self.gpu_id)
					for k in range(len(h_c)):
						h_c[k] = Variable(h_c[k])
						if self.gpu_id>=0:
							h_c[k] = h_c[k].cuda(self.gpu_id)
							

					if self.gpu_id >= 0:
						word_input = word_input.cuda(self.gpu_id)
						char_input = char_input.cuda(self.gpu_id)
						labels = labels.cuda(self.gpu_id) 
						feat_vector = feat_vector.cuda(self.gpu_id)


					if len(h_w)==1:
						h_w=h_w[0]
					if len(h_c)==1:
						h_c=h_c[0]

					scores, soft_scores, h_w, h_c = self.model(word_input, char_input, \
						labels, word_lens, char_lens,h_w,h_c, feat_vector)
					labels = labels[:,self.class_ind].long()
					loss_weighted = self.loss_weighted(scores,labels)
					loss_weighted.backward()
					epoch_loss+=loss_weighted.data[0]

					if type(h_w) is not tuple:
						h_w = [h_w]
					if type(h_c) is not tuple:
						h_c = [h_c]
					for l, ex in enumerate(input_examples):
						if ex.id not in hidden_state:
							hidden_state[ex.id]=[[None]*len(h_w),[None]*len(h_c)]
						for k in range(len(h_w)):
							hidden_state[ex.id][0][k] = h_w[k][:,l,:].data
						for k in range(len(h_c)):
							hidden_state[ex.id][1][k] = h_c[k][:,l,:].data


					#clip gradients
					torch.nn.utils.clip_grad_norm(self.model.parameters(), self.max_clip_norm)

					self.optim.step()
					logger.log(self.id, 'train', epoch=epoch, it=total_iters, 
						loss=loss_weighted.data[0], part=part)
					sys.stdout.flush()
					total_iters+=1
					epoch_iters+=1

			val_loss, roc_score,conf_mat = self.eval(batch_size,epoch)
			logger.log(self.id, 'train_med', epoch=epoch, loss=epoch_loss*1.0/epoch_iters)

			if val_loss<best_val:
				best_val = val_loss
				self.model.save(os.path.join(self.model_path,'best.pt'))

			self.model.save(os.path.join(self.model_path,'%s.pt'%epoch))

	def eval(self, batch_size, epoch=0):
		self.model.eval()
		total_iters = 0
		total_loss = 0
		total_scores = [[],[]]
		hidden_state = {}
		for part in range(self.val_set.num_parts):
			part_len = self.val_set.part_len(part)
			iters = (part_len // batch_size) + (part_len%batch_size!=0)
			
			ids = np.random.permutation(part_len)
			for i in range(iters):

				h_w, h_c = self.model.zero_hidden(len(ids[i*batch_size:(i+1)*batch_size]), self.gpu_id)

				word_input, char_input, labels, word_lens, char_lens, input_examples, feat_vector = \
					self.val_set.get_batch_input(part, ids[i*batch_size:(i+1)*batch_size],
												 requires_grad = False)

				for l, ex in enumerate(input_examples):
					if ex.id in hidden_state:
						for k in range(len(h_w)):
							h_w[k][:,l,:] = hidden_state[ex.id][0][k]
						for k in range(len(h_c)):
							h_c[k][:,l,:] = hidden_state[ex.id][1][k]

				for k in range(len(h_w)):
					h_w[k] = Variable(h_w[k])
					if self.gpu_id>=0:
						h_w[k] = h_w[k].cuda(self.gpu_id)
				for k in range(len(h_c)):
					h_c[k] = Variable(h_c[k])
					if self.gpu_id>=0:
						h_c[k] = h_c[k].cuda(self.gpu_id)

				if self.gpu_id >= 0:
					word_input = word_input.cuda(self.gpu_id)
					char_input = char_input.cuda(self.gpu_id)
					labels = labels.cuda(self.gpu_id)
					feat_vector = feat_vector.cuda(self.gpu_id)


				scores, soft_scores, h_w, h_c = self.model(word_input, char_input, labels,
													word_lens, char_lens, h_w, h_c, feat_vector)
				labels = labels[:, self.class_ind]

				if type(h_w) is not tuple:
					h_w = [h_w]
				if type(h_c) is not tuple:
					h_c = [h_c]
				for l, ex in enumerate(input_examples):
					if ex.id not in hidden_state:
						hidden_state[ex.id]=[[None]*len(h_w),[None]*len(h_c)]
					for k in range(len(h_w)):
						hidden_state[ex.id][0][k] = h_w[k][:,l,:].data
					for k in range(len(h_c)):
						hidden_state[ex.id][1][k] = h_c[k][:,l,:].data
				


				loss_weighted = self.loss_val(scores,labels).data[0]
				logger.log(self.id, 'val', it=total_iters, 
					part=part, loss = loss_weighted)
				total_loss += loss_weighted
				total_iters+=1

				total_scores[0].extend(soft_scores.data[:,1])
				total_scores[1].extend(label.data[0] for label in labels)

		val_loss = total_loss / total_iters
		roc_score = roc_auc_score(total_scores[1],total_scores[0])
		conf_mat = confusion_matrix(total_scores[1], [x>0.5 for x in total_scores[0]])*1.0/len(total_scores[0])

		self.model.train()

		logger.log(self.id, 'val_med', epoch=epoch, loss=val_loss, roc_score=roc_score,
				z_tn=conf_mat[0][0], z_fp=conf_mat[0][1], z_fn=conf_mat[1][0], z_tp=conf_mat[1][1])

		return val_loss, roc_score, conf_mat

	def to_csv(self, dataset, batch_size):
		self.model.eval()
		probs = {}
		hidden_state = {}
		for part in range(dataset.num_parts):
			part_len = dataset.part_len(part)
			iters = (part_len // batch_size) + (part_len%batch_size!=0)
			
			ids = list(range(part_len))
			for i in range(iters):
				h_w, h_c = self.model.zero_hidden(len(ids[i*batch_size:(i+1)*batch_size]), self.gpu_id)

				word_input, char_input, labels, word_lens, char_lens, input_examples, feat_vector = \
					dataset.get_batch_input(part, ids[i*batch_size:(i+1)*batch_size],
												 requires_grad = False)

				for l, ex in enumerate(input_examples):
					if ex.id in hidden_state:
						for k in range(len(h_w)):
							h_w[k][:,l,:] = hidden_state[ex.id][0][k]
						for k in range(len(h_c)):
							h_c[k][:,l,:] = hidden_state[ex.id][1][k]

				for k in range(len(h_w)):
					h_w[k] = Variable(h_w[k])
					if self.gpu_id>=0:
						h_w[k] = h_w[k].cuda(self.gpu_id)
				for k in range(len(h_c)):
					h_c[k] = Variable(h_c[k])
					if self.gpu_id>=0:
						h_c[k] = h_c[k].cuda(self.gpu_id)


				if self.gpu_id >= 0:
					word_input = word_input.cuda(self.gpu_id)
					char_input = char_input.cuda(self.gpu_id)
					feat_vector = feat_vector.cuda(self.gpu_id)

				scores, soft_scores, h_w, h_c = self.model(word_input, char_input, labels, word_lens,
												 char_lens, h_w, h_c, feat_vector)
				soft_scores = soft_scores.data[:,1]
				for j, ex in enumerate(input_examples):
					if ex.id in probs:
						probs[ex.id] = max(probs[ex.id], soft_scores[j])
					else:
						probs[ex.id] = soft_scores[j]

				if type(h_w) is not tuple:
					h_w = [h_w]
				if type(h_c) is not tuple:
					h_c = [h_c]
				for l, ex in enumerate(input_examples):
					if ex.id not in hidden_state:
						hidden_state[ex.id]=[[None]*len(h_w),[None]*len(h_c)]
					for k in range(len(h_w)):
						hidden_state[ex.id][0][k] = h_w[k][:,l,:].data
					for k in range(len(h_c)):
						hidden_state[ex.id][1][k] = h_c[k][:,l,:].data

				print('part[%d] it/iters[%d/%d]'%(part,i,iters))
				sys.stdout.flush()

		pd.DataFrame.from_dict(probs, orient='index').round(4).to_csv('%s.csv'%self.id, 
			header=[str(self.class_ind)])
		self.model.train()

	def save_comment_embedding(self, dataset, batch_size, mode='train'):
		self.model.eval()
		out = []

		for part in range(dataset.num_parts):
			part_len = dataset.part_len(part)
			iters = (part_len // batch_size) + (part_len%batch_size!=0)

			ids = list(range(part_len))
			hidden_state = {}
			for i in range(iters):
				h_w, h_c = self.model.zero_hidden(len(ids[i * batch_size:(i + 1) * batch_size]),
												  self.gpu_id)

				word_input, char_input, labels, word_lens, char_lens, input_examples, feat_vector = \
					dataset.get_batch_input(part, ids[i*batch_size:(i+1)*batch_size],
												 requires_grad = False)

				for l, ex in enumerate(input_examples):
					if ex.id in hidden_state:
						for k in range(len(h_w)):
							h_w[k][:,l,:] = hidden_state[ex.id][0][k]
						for k in range(len(h_c)):
							h_c[k][:,l,:] = hidden_state[ex.id][1][k]

				for k in range(len(h_w)):
					h_w[k] = Variable(h_w[k])
					if self.gpu_id>=0:
						h_w[k] = h_w[k].cuda(self.gpu_id)
				for k in range(len(h_c)):
					h_c[k] = Variable(h_c[k])
					if self.gpu_id>=0:
						h_c[k] = h_c[k].cuda(self.gpu_id)

				if self.gpu_id >= 0:
					word_input = word_input.cuda(self.gpu_id)
					char_input = char_input.cuda(self.gpu_id)
					feat_vector = feat_vector.cuda(self.gpu_id)

				h_concat, h_w, h_c = self.model.forward_emb(word_input, char_input, word_lens, char_lens,
												  h_w, h_c, feat_vector)
				if self.gpu_id >= 0:
					h_concat = h_concat.cpu()

				if type(h_w) is not tuple:
					h_w = [h_w]
				if type(h_c) is not tuple:
					h_c = [h_c]
				for l, ex in enumerate(input_examples):
					if ex.id not in hidden_state:
						hidden_state[ex.id]=[[None]*len(h_w),[None]*len(h_c)]
					for k in range(len(h_w)):
						hidden_state[ex.id][0][k] = h_w[k][:,l,:].data
					for k in range(len(h_c)):
						hidden_state[ex.id][1][k] = h_c[k][:,l,:].data

					out.append([ex.id, ex.labels, h_concat[l].data.numpy()])

		serialize_to_file(out, 'data/' + mode + '_embeddings.pt')
