import torch
import torch.nn as nn
from torch.autograd import Variable

class SeqRNN(nn.Module):

	class InvalidCellTypeException(Exception):
		pass

	def __init__(self, input_size, embedding_size, hidden_size, pad_idx = 1,
								cell_type='lstm', n_layers=1, dropout_rate = 0.0, bi=False,
				 				embedding_matrix=None):
		super().__init__()
		self.input_size = input_size
		self.embedding_size = embedding_size
		self.hidden_size = hidden_size
		self.cell_type = cell_type.lower()
		self.n_layers = n_layers
		self.embedding = nn.Embedding(input_size,embedding_size)
		if embedding_matrix is not None:
			self.embedding.weight.data.copy_(embedding_matrix)
		self.drop = dropout_rate
		self.drop_layer = nn.Dropout(dropout_rate)

		if cell_type is 'gru':
			self.rnn = nn.GRU(embedding_size, hidden_size,n_layers, batch_first=True,
							  dropout=dropout_rate, bidirectional=bi)
		elif cell_type is 'lstm':
			self.rnn = nn.LSTM(embedding_size, hidden_size,n_layers, batch_first=True,
							   dropout=dropout_rate, bidirectional=bi)
		#else:
		#	raise InvalidCellTypeException()

	def forward(self, input, hidden=None):
		emb = self.drop_layer(self.embedding(input))
		output, hidden = self.rnn(emb,hidden)
		return output, hidden

	def zero_hidden(self, batch_size, gpu_id):
		input = Variable(torch.zeros([batch_size,1]).type(torch.LongTensor))
		if gpu_id>=0:
			input = input.cuda(gpu_id)
		o,h = self.forward(input)
		if type(h) is not tuple:
			h = [h]
		return [torch.zeros(v.size()) for v in h]



