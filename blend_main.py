import pandas as pd
from os import listdir
from os.path import join,splitext

subm_path='blend/subm/'

def get_subms(path):
	ret = {}
	for file in listdir(path):
		file_path = join(path,file)
		file_name = splitext(file_path)[0].split('/')[-1]
		ret[file_name]=pd.read_csv(file_path)
	return ret

def main():
	subms = get_subms(subm_path)
	n = len(subms)
	ret = subms[list(subms.keys())[0]].copy(deep=True)
	cols = ret.columns[1:]
	for c in cols:
		ret[c] = 0
	for key in subms:
		for c in cols:
			ret[c]=ret[c]+(1.0/n)*subms[key][c]
	ret.to_csv('submission.csv', index=False)

if __name__=='__main__':
	main()