import pandas as pd
import regex
import sys
import argparse
from unidecode import unidecode
import re


def upper_proc(text):
	text=str(text)
	total_chars = len(re.findall(r'[a-zA-Z]',text))
	if total_chars==0:
		return 0
	else:
		return len(re.findall(r'[A-Z]',text))*1.0/total_chars

def main(path):
	data=pd.read_csv(path+'.csv')
	data['upper'] = data['comment_text'].apply(upper_proc)
	data['comment_text'] = data['comment_text'].apply(lambda x: str(x).lower())
	data['comment_text'] = data['comment_text'].apply(lambda x: unidecode(x))
	data.to_csv(path+'_l.csv', index=False)

parser = argparse.ArgumentParser()
parser.add_argument('data',type=str)
args = parser.parse_args()
if args.data[-4:]=='.csv':
	args.data = args.data[:-4]
main(args.data)