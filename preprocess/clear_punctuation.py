import pandas as pd
import regex
import sys
import argparse


def get_ex(comments):
	ret = []
	for comment in comments:
		comment=str(comment)
		if comment.find('!!!!')!=-1:
			ret.append(1)
		elif comment.find('!!!')!=-1:
			ret.append(0.5)
		elif comment.find('!!')!=-1:
			ret.append(0.25)
		else:
			ret.append(0)
	return ret

def main(path):
	data=pd.read_csv(path+'.csv')
	data['exclamation'] = get_ex(data['comment_text'])
	data['comment_text'] = data['comment_text'].replace(r'\.',' . ',regex=True)
	data['comment_text'] = data['comment_text'].replace(r'[,;:]',' ',regex=True)
	data['comment_text'] = data['comment_text'].replace(r'!',' ! ',regex=True)
	data['comment_text'] = data['comment_text'].replace(r'\?',' ? ',regex=True)
	data['comment_text'] = data['comment_text'].replace(r'[\s]+',' ',regex=True)
	data.to_csv(path+'_p.csv', index=False)

parser = argparse.ArgumentParser()
parser.add_argument('data',type=str)
args = parser.parse_args()
if args.data[-4:]=='.csv':
	args.data = args.data[:-4]
main(args.data)