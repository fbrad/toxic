import pandas as pd
import argparse

rgx = '((http[s]?://)|(www))\
(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))*'


def main(path):
	data = pd.read_csv(path+'.csv')
	data['comment_text'] = data['comment_text'].replace(rgx,'',regex=True)
	data.to_csv(path+'_u.csv', index=False)

parser = argparse.ArgumentParser()
parser.add_argument('data',type=str)
args = parser.parse_args()
if args.data[-4:]=='.csv':
	args.data = args.data[:-4]
main(args.data)
