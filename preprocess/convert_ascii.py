import pandas as pd
import regex
import sys
import argparse
from unidecode import unidecode


def main(path):
	data=pd.read_csv(path+'.csv')
	data['comment_text'] = data['comment_text'].apply(lambda x: unidecode(str(x)))
	data.to_csv(path+'_a.csv', index=False)

parser = argparse.ArgumentParser()
parser.add_argument('data',type=str)
args = parser.parse_args()
if args.data[-4:]=='.csv':
	args.data = args.data[:-4]
main(args.data)