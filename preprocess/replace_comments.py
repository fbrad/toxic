import pandas as pd
import re
import argparse
import sys
import string

def main(data_path, from_path):
	data = pd.read_csv(data_path+'.csv')
	source = pd.read_csv(from_path)
	data['comment_text'] = source['comment_text']
	data.to_csv(data_path+'_com.csv',index=False)



parser = argparse.ArgumentParser()
parser.add_argument('data',type=str)
parser.add_argument('from_path',type=str)
args = parser.parse_args()
if args.data[-4:]=='.csv':
	args.data = args.data[:-4]
main(args.data, args.from_path)