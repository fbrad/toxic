import pandas as pd
import re
import argparse
import sys
import string

def main(data_path):
	data = pd.read_csv(data_path+'.csv')
	for c in string.ascii_lowercase:
		data['comment_text'] = data['comment_text'].replace(c+c+c+'[%s]*'%c,
			c,regex=True)
	data.to_csv(data_path+'_col.csv',index=False)



parser = argparse.ArgumentParser()
parser.add_argument('data',type=str)
args = parser.parse_args()
if args.data[-4:]=='.csv':
	args.data = args.data[:-4]
main(args.data)