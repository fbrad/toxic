import os
import argparse
import sys


def printD(*args, **kargs):
	print(*args,**kargs)
	sys.stdout.flush()

def main(data_path, d1_path,d2_path):
	# printD('Clear Tags')
	# os.system('python3 ./preprocess/clear_tags.py %s'%data_path)
	# data_path+='_t'

	# printD('Clear Urls')
	# os.system('python3 ./preprocess/clear_urls.py %s'%data_path)
	# data_path+='_u'

	# printD('Clear nonwestern')
	# os.system('python3 ./preprocess/clear_nonwestern.py %s'%data_path)
	# data_path+='_nw'

	printD('Convert Ascii')
	os.system('python3 ./preprocess/convert_ascii.py %s'%data_path)
	data_path+='_a'

	# printD('Clear Numbers')
	# os.system('python3 ./preprocess/clear_numbers.py %s'%data_path)
	# data_path+='_n'

	printD('Clear Punctuation')
	os.system('python3 ./preprocess/clear_punctuation.py %s'%data_path)
	data_path+='_p'

	# printD('Clear Stopwords')
	# os.system('python3 ./preprocess/clear_stopwords.py %s'%data_path)
	# data_path+='_s'

	# printD('Clear Mono')
	# os.system('python3 ./preprocess/clear_mono.py %s'%data_path)
	# data_path+='_m'

	# printD('Cumulate Underline')
	# os.system('python3 ./preprocess/cumulate_underline.py %s'%(data_path))
	# data_path+='_cu'

	printD('To Lower')
	os.system('python3 ./preprocess/lowercase.py %s'%data_path)
	data_path+='_l'

	# printD('Collapse 3+')
	# os.system('python3 ./preprocess/collapse.py %s'%(data_path))
	# data_path+='_col'

	# printD('Replace single')
	# os.system('python3 ./preprocess/replace.py %s %s'%(data_path,d1_path))
	# data_path+='_r'

	# printD('Replace double')
	# os.system('python3 ./preprocess/replace.py %s %s'%(data_path,d2_path))
	# data_path+='_r'




parser = argparse.ArgumentParser()
parser.add_argument('data',type=str)
parser.add_argument('d1',type=str)
parser.add_argument('d2',type=str)
args = parser.parse_args()
if args.data[-4:]=='.csv':
	args.data = args.data[:-4]
main(args.data,args.d1,args.d2)