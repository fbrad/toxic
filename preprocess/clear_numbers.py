import pandas as pd
import argparse


def main(path):
	data=pd.read_csv(path+'.csv')
	data['comment_text'] = data['comment_text'].\
		replace(r'([0-9][0-9]+)|(\b[0-9]\b)','',regex=True)
	data.to_csv(path+'_n.csv', index=False)

parser = argparse.ArgumentParser()
parser.add_argument('data',type=str)
args = parser.parse_args()
if args.data[-4:]=='.csv':
	args.data = args.data[:-4]
main(args.data)