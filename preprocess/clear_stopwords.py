import pandas as pd
import re
import argparse
import nltk
from nltk.corpus import stopwords

def main(path):
	data = pd.read_csv(path+'.csv')
	words = set(stopwords.words('english'))
	data['comment_text']= data['comment_text'].apply(lambda x: ' '.join([word 
		for word in str(x).split(' ') if word not in words]))
	data.to_csv(path+'_s.csv', index=False)



parser = argparse.ArgumentParser()
parser.add_argument('data',type=str)
args = parser.parse_args()
if args.data[-4:]=='.csv':
	args.data = args.data[:-4]
main(args.data)