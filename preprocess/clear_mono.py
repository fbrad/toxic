import pandas as pd
import re
import argparse
import nltk
from nltk.corpus import stopwords

def main(path):
	data = pd.read_csv(path+'.csv')
	data['comment_text']=data['comment_text'].replace(r'\b[a-z]\b','',regex=True)
	data.to_csv(path+'_m.csv', index=False)



parser = argparse.ArgumentParser()
parser.add_argument('data',type=str)
args = parser.parse_args()
if args.data[-4:]=='.csv':
	args.data = args.data[:-4]
main(args.data)