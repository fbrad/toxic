from re import split
import csv
import torch
import pickle as cPickle

def deserialize_from_file(path):
    f = open(path, 'rb')
    obj = cPickle.load(f)
    f.close()
    return obj

def serialize_to_file(obj, path, protocol=cPickle.HIGHEST_PROTOCOL):
    f = open(path, 'wb')
    cPickle.dump(obj, f, protocol=protocol)

def get_sentence_embedding(sentence, glove_dict, average=False):
    sum_word_emb = torch.zeros(300)
    num_toks = 0
    pattern = '([\[\](){}*@&=#,.:;%$\\\'`^"_\-–²+|<>?!/ ])'
    toks = [tok.lower() for tok in split(pattern, sentence) if tok != '' and tok != ' ']
    #average token embedding
    for tok in toks:
        if tok in glove_dict:
            sum_word_emb += glove_dict[tok]
            num_toks += 1
        else:
            #print("unknown: ", tok)
            pass
        
    if average and num_toks > 0:
        sum_word_emb = sum_word_emb / num_toks
            
    return sum_word_emb.numpy()

def read_csv_file(csv_path, offset=0):
	data = csv.reader(open(csv_path, 'r'))
	entries = []
	header = data.__next__()

	for row in data:
		entries.append(row[offset:])

	return entries

def save_embeddings(csv_entries, glove_dict, output_name):
	entry_embeddings = []
	total = len(csv_entries)
	for idx,entry in enumerate(csv_entries):
		entry_id = entry[0]
		sentence = entry[1]
		labels = entry[2:]
		entry_embeddings.append([entry_id, labels, [get_sentence_embedding(sentence, glove_dict)]])
		if idx % 1000 == 0:
			print(idx, '/', total)
	
	serialize_to_file(entry_embeddings, output_name)
	

if __name__ == '__main__':
	print("Reading Glove embeddings")
	glove_dict = deserialize_from_file('../data/glove_embeddings.bin')
	print("Reading csv entries")
	entries = read_csv_file('../data/test.csv')

	save_embeddings(entries, glove_dict, '../data/test_embeddings.bin')
