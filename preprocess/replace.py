import pandas as pd
import re
import argparse
import sys

def main(data_path, list_path):
	data = pd.read_csv(data_path+'.csv')
	f=open(list_path,'r')
	for (i,line) in enumerate(f.readlines()):
		print(i)
		sys.stdout.flush()
		words=[x for x in line.split(':') if len(x)>0]
		data['comment_text'] = data['comment_text'].replace(r'\b'+words[0]+r'\b',
				' '+words[1]+' ',regex=True)
	data['comment_text'] = data['comment_text'].replace(r'[\s]+',
		' ',regex=True)
	data.to_csv(data_path+'_r.csv', index=False)



parser = argparse.ArgumentParser()
parser.add_argument('data',type=str)
parser.add_argument('lst',type=str)
args = parser.parse_args()
if args.data[-4:]=='.csv':
	args.data = args.data[:-4]
main(args.data,args.lst)