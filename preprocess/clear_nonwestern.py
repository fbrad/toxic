import pandas as pd
import regex
import sys
import argparse


def main(path):
	data=pd.read_csv(path+'.csv')
	for i in range(len(data)):
		if type(data['comment_text'][i]) is float:
			continue
		if regex.search(r'[^\p{Common}\p{Latin}]',
		data['comment_text'][i]) is not None:
			data.ix[i,'comment_text']=regex.sub(r'[^\p{Common}\p{Latin}]',
				'',data['comment_text'][i])
		if i%100 is 0:
			print(i)
			sys.stdout.flush()
	data.to_csv(path+'_nw.csv', index=False)

parser = argparse.ArgumentParser()
parser.add_argument('data',type=str)
args = parser.parse_args()
if args.data[-4:]=='.csv':
	args.data = args.data[:-4]
main(args.data)