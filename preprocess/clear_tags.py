import pandas as pd
import re
import argparse

tags=[
('\[\[','\]\]'),
('<','>'),
('\{\{','\}\}')
]

def main(path):
	data = pd.read_csv(path+'.csv')
	for tag in tags:
		data['comment_text'] = data['comment_text'].replace(r'(%s).*?(%s)'
			%(tag[0],tag[1]),'',regex=True)
	data.to_csv(path+'_t.csv', index=False)



parser = argparse.ArgumentParser()
parser.add_argument('data',type=str)
args = parser.parse_args()
if args.data[-4:]=='.csv':
	args.data = args.data[:-4]
main(args.data)