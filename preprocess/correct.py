import pandas as pd
import regex
import sys
import argparse
import numpy as np
import enchant
import editdistance
import termios
import tty

INF = 1000
mat = np.zeros([INF,INF],dtype=np.int32)


def getch():
	fd = sys.stdin.fileno()
	old_settings = termios.tcgetattr(fd)
	try:
		tty.setraw(fd)
		ch = sys.stdin.read(1)
	finally:
		termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
	return ch

def get_dict(path):
	f = open(path,'r')
	ret = []
	for line in f.readlines():
		ret.append(line.strip())
	return ret

def edit_dist(s1,s2):
	global mat
	for i in range(len(s1)):
		for j in range(len(s2)):
			if s1[i]==s2[j]:
				mat[i+1][j+1]=mat[i][j]
			else:
				mat[i+1][j+1]=min(mat[i+1][j],mat[i][j+1])+1
	return mat[len(s1)][len(s2)]

def min_dist(word):
	if len(word)<=3:
		return 0
	else:
		return 1

def get_cor(word,lst,words,ind_w,ind_c):
	best=1000
	match=''
	for cand in lst:
		if cand[0]!=word[0] or cand[-1]!=word[-1]:
			continue
		dist = editdistance.eval(cand,word)
		if dist<=min_dist(word) and dist<best:
			best=dist
			match=cand
	if len(match) is 0 or best is 0:
		return 0, word
	else:
		print('---------------------')
		print(ind_c)
		print(' '.join(words[max(ind_w-4,0):min(ind_w+4,len(words))]))
		print('%s -> %s (y/n/a(yes all)/o(no all))'%(word,match))
		sys.stdout.flush()
		c=getch()
		print('---------------------')

		if c[0] is 'a':
			return 0, match
		elif c[0] is 'o':
			return 0, word
		elif c[0] is 'n':
			return -1, word
		else:
			return -1, match

def get_cor_aux(word,lst,words,ind_w,ind_c):
	best=1000
	match=''
	for cand in lst:
		if cand[0]!=word[0] or cand[-1]!=word[-1]:
			continue
		dist = editdistance.eval(cand,word)
		if dist<=min_dist(word) and dist<best:
			best=dist
			match=cand
	if len(match) is 0 or best is 0:
		return 0, word
	else:
		print('---------------------')
		print(ind_c)
		print(' '.join(words[max(ind_w-4,0):min(ind_w+4,len(words))]))
		print('%s -> %s (a(yes all)/o(no all))'%(word,match))
		sys.stdout.flush()
		c=getch()
		print('---------------------')

		if c[0] is 'a':
			return 1, match
		elif c[0] is 'o':
			return -1, word


def get_cor_bi(word,lst,words,ind_w,ind_c):
	best=1000
	match=''
	for cand in lst:
		if cand[0]!=word[0]:
			continue
		dist = editdistance.eval(cand,word)
		if dist<=min_dist(word) and dist<best:
			best=dist
			match=cand
	if len(match) is 0:
		return 0, word
	else:
		print('---------------------')
		print(ind_c)
		print(' '.join(words[max(ind_w-4,0):min(ind_w+5,len(words))]))
		print('%s -> %s (y/n)'%(word,match))
		sys.stdout.flush()
		c=getch()
		print('---------------------')

		if c[0] is 'n':
			return 0, word
		else:
			return 1, match



def count(data_path, dict_path):
	data=pd.read_csv(data_path+'.csv')
	d = get_dict(dict_path)
	de = enchant.Dict("en_US")
	hist = {}
	count=0
	for (i,comment) in enumerate(data['comment_text']):
		if type(comment) is float or len(comment) is 0:
			continue
		found=False
		words=comment.split(' ')
		for (j,word) in enumerate(words):
			if len(word) is 0:
				continue
			if word in hist:
				pass
			else:
				if not de.check(word):
					for bw in d:
						if bw[0]!=word[0] or bw[-1]!=word[-1]:
							continue
						if editdistance.eval(word,bw)<=min_dist(word):
							found=True
							count+=1
							#print(word, bw)
				else:
					hist[word]=word
		if i%100 is 0:
			print(i)
			sys.stdout.flush()
		#if found:
			#print(comment)
	print(count)
	#data.to_csv(path+'_p.csv')

def cor_single(data_path,dict_path):
	data=pd.read_csv(data_path+'.csv')
	d_bad = get_dict(dict_path)
	d_en = enchant.Dict("en_US")
	hist = {}
	for (i,comment) in enumerate(data['comment_text']):
		comment = str(comment)
		cor = []
		words=comment.split(' ')
		change=False
		for (j,word) in enumerate(words):
			if len(word) is 0:
				continue
			if word in hist and hist[word]!=0:
				cor.append(hist[word])
			else:
				if d_en.check(word):
					hist[word]=word
					cor.append(word)
				else:
					c, w = get_cor(word,d_bad,words,j,i)
					if c is 0:
						hist[word]=w
						cor.append(w)
					else:
						cor.append(w)		
					if w!=word:
						change=True
		if change:
			data.ix[i,'comment_text']=' '.join(cor)
			print(data['comment_text'][i])
		# if i%100 is 0:
		# 	print(i)
		# 	sys.stdout.flush()
	data.to_csv(data_path+'_c.csv', index=False)

def cor_single_aux(data_path,dict_path):
	data=pd.read_csv(data_path+'.csv')
	d_bad = get_dict(dict_path)
	d_en = enchant.Dict("en_US")
	hist = {}
	for (i,comment) in enumerate(data['comment_text']):
		comment = str(comment)
		words=comment.split(' ')
		for (j,word) in enumerate(words):
			if len(word) is 0:
				continue
			if word in hist and hist[word]!=0:
				pass
			else:
				if d_en.check(word):
					hist[word]=word
					cor.append(word)
				else:
					c, w = get_cor(word,d_bad,words,j,i)
					if c is 0:
						hist[word]=w
						cor.append(w)
					else:
						cor.append(w)		
					if w!=word:
						change=True
		if change:
			data.ix[i,'comment_text']=' '.join(cor)
			print(data['comment_text'][i])
		# if i%100 is 0:
		# 	print(i)
		# 	sys.stdout.flush()
	data.to_csv(data_path+'_c.csv', index=False)

def count_bi(data_path,dict_path):
	data=pd.read_csv(data_path+'.csv')
	d_bad = get_dict(dict_path)
	d_en = enchant.Dict("en_US")
	count=0
	for (i,comment) in enumerate(data['comment_text']):
		comment = str(comment)
		cor = []
		words=comment.split(' ')
		change=False
		for (j,word) in enumerate(words):
			if j>=len(words)-1 or len(word)*len(words[j+1]) is 0:
				continue
			word=word+words[j+1]
			if len(word)>8:
				continue
			for bw in d_bad:
				if bw[0]!=word[0]:
					continue
				if editdistance.eval(word,bw)<=min_dist(word):
					count+=1
			
	
		if i%100 is 0:
			print(i)
			sys.stdout.flush()
	print(count)

def cor_bi(data_path,dict_path):
	data=pd.read_csv(data_path+'.csv')
	d_bad = get_dict(dict_path)
	d_en = enchant.Dict("en_US")
	count=0
	for (i,comment) in enumerate(data['comment_text']):
		comment = str(comment)
		cor = []
		words=comment.split(' ')
		words=list(filter(lambda x: len(x)>0, words))
		change=False
		j = 0
		while j<=len(words)-1:
			if j==len(words)-1:
				cor.append(words[j])
				break
			word=words[j]+words[j+1]
			if len(word)>8:
				cor.append(words[j])
				j+=1
				continue
			c,w = get_cor_bi(word,d_bad,words,j,i)
			if c is 0:
				cor.append(words[j])
				j+=1
			else:
				j+=2
				change=True
				cor.append(w)
		if change:
			data.ix[i,'comment_text']=' '.join(cor)
			print(data['comment_text'][i])
	data.to_csv(data_path+'_c2.csv', index=False)

def cor_single_aux(data_path,dict_path):
	data=pd.read_csv(data_path+'.csv')
	d_bad = get_dict(dict_path)
	d_en = enchant.Dict("en_US")
	hist = {}
	cor = {}
	for (i,comment) in enumerate(data['comment_text']):
		comment = str(comment)
		words=comment.split(' ')
		found=False
		for (j,word) in enumerate(words):
			if len(word) is 0:
				continue
			if word in hist and hist[word]!=0:
				pass
			else:
				if d_en.check(word):
					hist[word]=word
				else:
					c, w = get_cor_aux(word,d_bad,words,j,i)
					if c is -1:
						hist[word]=word
					elif c is 1:
						hist[word]=w
						cor[word]=w
						found=True
	f = open(data_path+'_cor.txt','w')
	for key in cor:
		print('%s:%s'%(key,cor[key]),file=f)
	f.close()

def cor_bi_aux(data_path,dict_path):
	data=pd.read_csv(data_path+'.csv')
	d_bad = get_dict(dict_path)
	d_en = enchant.Dict("en_US")
	count=0
	cor = {}
	for (i,comment) in enumerate(data['comment_text']):
		comment = str(comment)
		words=comment.split(' ')
		words=list(filter(lambda x: len(x)>0, words))
		change=False
		j = 0
		while j<len(words)-1:
			word=words[j]+words[j+1]
			if len(word)>8 or '%s %s'%(words[j],words[j+1]) is in cor:
				j+=1
				continue
			c,w = get_cor_bi(word,d_bad,words,j,i)
			if c is 0:
				j+=1
			else:
				change=True
				cor['%s %s'%(words[j],words[j+1])]=w
				j+=2		
	f = open(data_path+'_cor2.txt','w')
	for key in cor:
		print('%s:%s'%(key,cor[key]),file=f)
	f.close()
					


parser = argparse.ArgumentParser()
parser.add_argument('data',type=str)
parser.add_argument('dict',type=str)
args = parser.parse_args()
if args.data[-4:]=='.csv':
	args.data = args.data[:-4]
cor_bi_aux(args.data,args.dict)