import argparse
import os
import csv
import torch
import pickle as cPickle
import string
from dataset_keras import Dataset, Example
from torchtext.vocab import Vocab
from collections import Counter, defaultdict
from nltk import word_tokenize
import nltk
from keras.preprocessing import text

"""
Usage: python preproc.py -train_file data/train.csv -output_dir data

Outputs: - data/vocab.pt, which contains word->token_id dictionary
         - data/train.pt, which contains the train data
         - data/val.pt, which contains the evaluation data
"""

parser = argparse.ArgumentParser(description='preproc.py',
								 formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument('-train_file', type=str, default='data/train_preproc.csv',
					help='path to training .csv file')
parser.add_argument('-test_file', type=str, default='data/test_preproc.csv',
					help='path to training .csv file')
parser.add_argument('-vocab_path', type=str, default='data/vocab.pt',
					help='path to Vocab instance')
parser.add_argument('-wordvec_path', type=str, default='data/glove.840B.300d.txt',
					help='path to word vectors text file')
parser.add_argument('-output_dir', type=str, default='data/',
					help='path to vocab and input files')
parser.add_argument('-min_freq', type=int, default=10,
					help='minimum frequency of word in order to be added to the vocabulary')
parser.add_argument('-max_words', type=int, default=200)
parser.add_argument('-max_word_chars', type=int, default=10)


def __getstate__(self):
	return dict(self.__dict__, stoi=dict(self.stoi))


def __setstate__(self, state):
	self.__dict__.update(state)
	self.stoi = defaultdict(lambda: 0, self.stoi)


Vocab.__getstate__ = __getstate__
Vocab.__setstate__ = __setstate__


def serialize_to_file(obj, path, protocol=cPickle.HIGHEST_PROTOCOL):
	f = open(path, 'wb')
	cPickle.dump(obj, f, protocol=protocol)
	f.close()


def deserialize_from_file(path):
	f = open(path, 'rb')
	obj = cPickle.load(f)
	f.close()
	return obj


# return a list of rows from the csv, where each row is also a list
def read_csv_file(csv_path, offset=0):
	data = csv.reader(open(csv_path, 'r'))
	entries = []
	header = data.__next__()

	for i, row in enumerate(data):
		entries.append(row[offset:])
		if i>100:
			break
	return entries


"""
    entries: list of lists containing the rows in the .csv
    Returns a Vocab object (<unk> has index 0, <pad> has index 1)
"""


def create_vocab(entries):
	tokenizer = text.Tokenizer(num_words=100000, lower=True)
	tokenizer.fit_on_texts([e[1] for e in entries])
	return tokenizer

"""
    creates the vocabulary used for encoding characters into char-rnn
"""


def create_char_vocab():
	cnt = Counter(list(string.printable))
	vocab = Vocab(cnt)

	return vocab


"""
    entries: rows from the .csv file
    vocab: precomputed vocabulary (from entries)
    Returns a .t7 file with train/val/test datasets
"""


def prepare_input(entries, vocab):
	data_tokens = []
	data_labels = []

	num_entries = len(entries)
	for idx, entry in enumerate(entries):
		tokens = [tok for tok in word_tokenize(entry[1]) if tok]
		tokens_id = [vocab.stoi[tok] for tok in tokens]
		labels = [int(c) for c in entry[2:]]
		if idx % 1000 == 0:
			print("[prepare_input] ", idx, "/", num_entries, " completed")

		data_tokens.append(tokens_id)
		data_labels.append(labels)

	return (data_tokens, data_labels)


# serialize_to_file((data_tokens, data_labels), "data/input.pt")

"""
    Converts a dataset stored as a list to Tensors.
    dataset : [[tok1, tok2, ...],[tok1, tok2, ...], ...]
    labels: [[class1, class2, ..., class6], [class1, class2, ...]]
"""


def convert_list_to_tensor(dataset, labels, vocab):
	max_len = max([len(entry) for entry in dataset])
	torch_dataset = torch.zeros(len(dataset), max_len).long().fill_(vocab.stoi['<pad>'])
	torch_labels = torch.zeros(len(dataset), 6).long()

	for idx, entry in enumerate(dataset):
		torch_dataset[idx, 0:len(entry)] = torch.LongTensor(entry)
		torch_labels[idx] = torch.LongTensor(labels[idx])

	return (torch_dataset, torch_labels)


"""
    train_per: percentage of data kept for training the model
    val_per: percentage of data kept for evaluating the model
    x: array of entries
"""


def split_data(x, y, train_per=0.9, val_per=0.1):
	data_size = len(x)
	stop_idx_train = int(train_per * data_size)
	# stop_idx_val = int(stop_idx_train + val_per * data_size)

	return (x[0:stop_idx_train], y[0:stop_idx_train], x[stop_idx_train:], y[stop_idx_train:])


"""
    x: data
    y: labels
    Returns (x_toxic, y_toxic, x_non_toxic, y_non_toxic)
"""


def split_data_by_toxicity(x, y):
	y_binary = [int(sum(labels) > 0) for labels in y]

	x_non_toxic, y_non_toxic, x_toxic, y_toxic = [], [], [], []
	for (x_i, y_i, y_bin_i) in zip(x, y, y_binary):
		if y_bin_i:
			x_toxic.append(x_i)
			y_toxic.append(y_i)
		else:
			x_non_toxic.append(x_i)
			y_non_toxic.append(y_i)

	return (x_non_toxic, y_non_toxic, x_toxic, y_toxic)


"""
    Splits data in train/eval such that the ratio of toxicity is the same in each dataset.
"""


def split_data_evenly(x, y):
	(x_non_toxic, y_non_toxic, x_toxic, y_toxic) = split_data_by_toxicity(x, y)

	(x_non_toxic_train, y_non_toxic_train, x_non_toxic_eval, y_non_toxic_eval) = \
		split_data(x_non_toxic, y_non_toxic)

	(x_toxic_train, y_toxic_train, x_toxic_eval, y_toxic_eval) = split_data(x_toxic, y_toxic)

	x_train = x_non_toxic_train + x_toxic_train
	y_train = y_non_toxic_train + y_toxic_train
	x_eval = x_non_toxic_eval + x_toxic_eval
	y_eval = y_non_toxic_eval + y_toxic_eval

	return (x_train, y_train, x_eval, y_eval)


def prepare_vocab_and_dataset(args):
	# create vocabulary
	entries = read_csv_file(args.train_file)
	examples = [Example(e) for e in entries]
	vocab = create_vocab(entries)

	# save vocabulary
	serialize_to_file(vocab, args.output_dir + 'vocab.pt')

	# create training/validation inputs
	dataset = Dataset(examples, vocab, args.max_words, args.max_word_chars)

	train, val = dataset.split_train_val()

	serialize_to_file(train, args.output_dir + 'train.pt')
	serialize_to_file(val, args.output_dir + 'val.pt')


def prepare_test_dataset(args):
	vocab = deserialize_from_file(args.output_dir + 'vocab.pt')

	entries = read_csv_file(args.test_file)
	examples = [Example(e) for e in entries]

	test = Dataset(examples, vocab, args.max_words, args.max_word_chars)

	serialize_to_file(test, args.output_dir + 'test.pt')



def prepare_embedding_matrix(args):
	vocab = deserialize_from_file(args.vocab_path)
	with open(args.wordvec_path) as f:
		embeddings = torch.zeros(((len(vocab.word_index) + 1), 300))
		for i,line in enumerate(f.readlines()):
			values = line.strip().split(' ')
			word = values[0]
			if word in vocab.word_index:
				index = vocab.word_index[word]
			else:
				index = None
			if index:
				print(values[0])
				glove_vector = [float(s) for s in values[1:]]
				vector = torch.FloatTensor(glove_vector)
				embeddings[index] = vector
			if i==100:
				break
		#return embeddings
		serialize_to_file(embeddings, 'data/embedding_matrix.pt')


if __name__ == '__main__':
	# nltk.download('punkt')
	args = parser.parse_args()  # prepare_vocab_and_dataset(args)
	prepare_vocab_and_dataset(args)
	prepare_test_dataset(args)
	prepare_embedding_matrix(args)
# data = deserialize_from_file('data/train.pt')
# print(data.examples[0])
