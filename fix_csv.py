import os

files = ['xgb_0','xgb_1','xgb_2','xgb_3','xgb_4','xgb_5']

def main():
	for file in files:
		f=open(file+'.csv','r')
		f2=open(file+'_f.csv','w')
		for line in f.readlines():
			if len(line)>0:
				x = float(line)
				print('%.6f'%x,file=f2)
		f2.close()
		f.close()
		os.system('mv %s_f.csv %s.csv'%(file,file))

if __name__=='__main__':
	main()