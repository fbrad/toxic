import torch.nn as nn
from torch.autograd import Variable
from models.seq_rnn import SeqRNN
import torch


class Model(nn.Module):
	def __init__(self, word_embed_size, char_embed_size, rnn_size, final_hidden, n_features,
				 word_vocab, char_vocab, n_layers, gpu_id, dropout_rate):
		super().__init__()

		self.word_embed_size = word_embed_size
		self.char_embed_size = char_embed_size
		self.rnn_size = rnn_size
		self.final_hidden_size= final_hidden
		self.n_features = n_features
		self.depth = n_layers
		self.drop = dropout_rate

		self.word_rnn = SeqRNN(len(word_vocab.itos), word_embed_size, rnn_size,
								 n_layers=n_layers, dropout_rate=dropout_rate)
		self.char_rnn = SeqRNN(len(char_vocab.itos), char_embed_size, rnn_size,
								 n_layers=n_layers, dropout_rate=dropout_rate)

		self.tanh_unit = nn.Tanh()
		self.hidden_trans = nn.Linear(rnn_size+rnn_size, final_hidden)

		self.feature_trans = nn.Linear(n_features, final_hidden)

		self.output = nn.Linear(2*final_hidden, 2)

		self.soft = nn.Softmax(1)

		self.gpu_id = gpu_id

	# inputs: (word_indices, char_indices, labels, word_lens, char_lens)
	# data: Variable(Tensor(batch_size, max_sequence_len))
	# labels: Variable(Tensor(batch_size))
	def forward(self, word_input, char_input, labels, word_lens, char_lens, 
							word_hidden=None, char_hidden=None, features_vec = None):
		# batch x sequence_len x embed_dim
		# print("[model.fwd] word_input = ", word_input)
		output_word, hidden_word = self.word_rnn(word_input, word_hidden)
		output_char, hidden_char = self.char_rnn(char_input, char_hidden)

		selector_word = Variable(torch.LongTensor(word_lens).unsqueeze(1).\
			expand(-1, self.rnn_size).unsqueeze(1)) - 1
		if self.gpu_id >= 0:
			selector_word = selector_word.cuda(self.gpu_id)

		selector_char = Variable(torch.LongTensor(char_lens).unsqueeze(1).\
			expand(-1, self.rnn_size).unsqueeze(1)) - 1
		if self.gpu_id >= 0:
			selector_char = selector_char.cuda(self.gpu_id)

		bottom_hidden_word = output_word.gather(1, selector_word).squeeze(1)
		bottom_hidden_char = output_char.gather(1, selector_char).squeeze(1)

		# batch x final_hidden
		hid_trans = self.tanh_unit(self.hidden_trans(torch.cat((bottom_hidden_word,
																bottom_hidden_char), 1)))

		feat_trans = self.tanh_unit(self.feature_trans(features_vec))

		scores = self.output(torch.cat((hid_trans,feat_trans), -1))

		return scores, self.soft(scores), hidden_word, hidden_char

	def forward_emb(self, word_input, char_input, word_lens, char_lens,
							word_hidden=None, char_hidden=None, features_vec = None):
		# batch x sequence_len x embed_dim
		# print("[model.fwd] word_input = ", word_input)
		output_word, hidden_word = self.word_rnn(word_input, word_hidden)
		output_char, hidden_char = self.char_rnn(char_input, char_hidden)

		selector_word = Variable(torch.LongTensor(word_lens).unsqueeze(1).\
			expand(-1, self.rnn_size).unsqueeze(1)) - 1
		if self.gpu_id >= 0:
			selector_word = selector_word.cuda(self.gpu_id)

		selector_char = Variable(torch.LongTensor(char_lens).unsqueeze(1).\
			expand(-1, self.rnn_size).unsqueeze(1)) - 1
		if self.gpu_id >= 0:
			selector_char = selector_char.cuda(self.gpu_id)

		bottom_hidden_word = output_word.gather(1, selector_word).squeeze(1)
		bottom_hidden_char = output_char.gather(1, selector_char).squeeze(1)

		# batch x final_hidden
		return torch.cat((bottom_hidden_word, bottom_hidden_char), 1), hidden_word, hidden_char

	def save(self, model_path):
		torch.save(self.state_dict(), model_path)

	def load(self, model_path, gpu_id):
		if gpu_id==-1:
			dev = 'cpu'
		else:
			dev = 'cuda:%d'%gpu_id
		dev_map = {'cpu':dev}
		for i in range(6):
			dev_map['cuda:%d'%i]=dev
		self.load_state_dict(torch.load(model_path, map_location = dev_map))

	def zero_hidden(self, batch_size, gpu_id):
		return self.word_rnn.zero_hidden(batch_size, gpu_id), \
			self.char_rnn.zero_hidden(batch_size, gpu_id)

	@staticmethod
	def test_forward():
		char_vocab = object()
		word_vocab = object()
		char_vocab.itos = 100
		word_vocab.itos = 100

		model = Model(100, 200, 10, vocab)

		data = Variable(torch.LongTensor([[1, 2, 3], [2, 0, 1]]))
		labels = Variable(torch.LongTensor([0, 1]))

		scores = model.forward((data, labels))

		print(scores)


if __name__ == '__main__':
	Model.test_forward()
