import subprocess
import argparse
import re
import os


def call_cmd(args):
	cmd = ['python3','main.py']
	env = os.environ.copy()
	if args.conv:
		cmd.append('-conv')
	if args.test:
		cmd.append('-test')
	if args.eval:
		cmd.append('-eval')
	if args.save_emb:
		cmd.append('-save_emb')
	if args.config is not None:
		for i, conf in enumerate(args.config):
			cmd_aux = list(cmd)
			f = open(conf,'r')
			
			for line in f.readlines():
				line=line.strip()
				arg = [arg.strip() for arg in re.split(':|=',line)]
				print(arg)
				if len(arg)<2:
					continue
				if arg[0]=='gpu_id' and arg[1]!='-1':
					env['CUDA_VISIBLE_DEVICES']=arg[1]
				
				cmd_aux.append('-'+arg[0])
				if arg[0]=='gpu_id' and arg[1]!='-1':
					cmd_aux.append('0')
				else:
					cmd_aux.append(arg[1])
			f.close()
			if args.load:
				cmd_aux.append('-load')
				cmd_aux.append(args.load[i])
			subprocess.Popen(cmd_aux, env=env)
	else:
		subprocess.Popen(cmd, env=env)


if __name__=='__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('-config',nargs='*', default=None)
	parser.add_argument('-test', action='store_true')
	parser.add_argument('-eval', action='store_true')
	parser.add_argument('-conv', action='store_true')
	parser.add_argument('-save_emb', action='store_true')
	parser.add_argument('-load', nargs='*', default=None)
	args=parser.parse_args()
	call_cmd(args)
	
